import { AlbumSearchResult, Album } from "../model/Search"


export const searchAlbums = (query: string) => {
  return fetch('https://api.spotify.com/v1/search?type=album&q=' + query, {
    headers: {
      Authorization: 'Bearer ' + getToken()
    }
  })
    .then(resp => resp.json())
    .then((resp: AlbumSearchResult) => resp.albums.items)
}

export const getAlbumById = (id: string) => {
  return fetch(`https://api.spotify.com/v1/albums/${id}`, {
    headers: {
      Authorization: 'Bearer ' + getToken()
    }
  })
    .then(resp => resp.json())
    .then((resp: Album) => resp)
}

let token: string | null = ''
if (!token) {
  token = JSON.parse(sessionStorage.getItem('token') || '')
}

if (!token && window.location.hash) {
  const params = new URLSearchParams(window.location.hash.slice(1))
  token = params.get('access_token')
  sessionStorage.setItem('token', JSON.stringify(token))
}

export const authorize = () => {
  const params = new URLSearchParams({
    client_id: '7bc00fce3a3747c69381eb9f0b069c98',
    response_type: 'token',
    redirect_uri: 'http://localhost:3000/',
    show_dialog: 'true'
  })

  window.location.href = 'https://accounts.spotify.com/authorize?' + params
}

export const getToken = () => {
  if (!token) {
    authorize()
  }
  return token
}