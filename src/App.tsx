import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import SearchView from './views/SearchView';
// import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AlbumDetails from './views/AlbumDetails';

function App() {
  return (
    <div>
      {/* .container>.row>.col */}

      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Lubie Sages!</h1>
            <Router>
              <Switch>
                <Route path="/" exact={true} component={SearchView} />
                <Route path="/album/:album_id" component={AlbumDetails} />
              </Switch>
            </Router>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
