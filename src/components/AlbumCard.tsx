import React from 'react';
import { Album } from '../model/Search';
import './AlbumCard.css'
import { Link } from 'react-router-dom'

interface Props {
  album: Album
}

const AlbumCard = ({ album }: Props) => {
  return (
    <div className="card">
      <img src={album.images[0].url}
        className="card-img-top" />

      <div className="card-body">
        <h5 className="card-title">
          <Link to={'/album/' + album.id} >
            {album.name}
          </Link>
        </h5>


      </div>
    </div>
  );
};
export default AlbumCard;
