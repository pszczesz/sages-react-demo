import React from 'react'
import AlbumCard from './AlbumCard'
import { Album } from '../model/Search'

interface Props {
  results: Album[]
}

const SearchResults = ({ results }: Props) => {

  return (
    <div>
      <div className="card-group">
        {
          results.map(result => {
            return <AlbumCard album={result} key={result.id} />
          })
        }
      </div>
    </div>
  )
}

export default SearchResults



